--- day 1
+ Cách tạo component: 2 cách ( rfc, rcc)
+ Data Binding: đưa data lên layout
+ RenderWithMap : convert array chứa data trở thành array chứa các component HTML, sau đó đưa lên layout
+ State: quản lý data ảnh hưởng đến việc render lại layout, layout sẽ được re-render khi data này được update
+ Props: interaction ~ tương tác giữa các component ~ truyền dữ liệu