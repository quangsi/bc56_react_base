import React, { Component } from "react";

export default class UserCard extends Component {
  // this.props ~ object ~ nhận data được truyền từ BÊN NGOÀI vào compmonent
  render() {
    // faker.js npm
    console.log(this.props);
    return (
      <div>
        <div class="card text-left">
          <div class="card-body">
            <h4 class="card-title">{this.props.title}</h4>
            <p class="display-4">{this.props.userData}</p>
            <button onClick={this.props.handleChange} className="btn btn-danger">
              Change username
            </button>
          </div>
        </div>
      </div>
    );
  }
}
