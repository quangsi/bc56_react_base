import React, { Component } from "react";
import UserCard from "./UserCard";
import { faker } from "@faker-js/faker";

export default class DemoProps extends Component {
  state = {
    user: "Alice",
  };
  //   state ở đâu , this.setState tại đó
  handleChangeName = () => {
    this.setState({ user: faker.animal.bird() });
  };
  render() {
    // let user = "Alice";
    return (
      <div>
        <h2>DemoProps</h2>
        <UserCard userData={this.state.user} title="Profile" handleChange={this.handleChangeName} />
      </div>
    );
  }
}
// one way binding in react
