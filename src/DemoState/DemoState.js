import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    number: 1,
    user: "Alice",
  };
  //   this.setState : dùng để udpate giá trị của state, this.setState là bất đồng bộ
  handleIncrease = () => {
    this.setState(
      {
        number: this.state.number + 1,
      },
      () => {
        console.log("tăng", this.state.number);
      }
    );
  };
  handleDecrease = () => {
    this.setState({
      number: this.state.number - 1,
    });
    console.log("giảm");
  };
  handleChangeName = (name) => {
    this.setState({ user: name });
  };
  //   function: có tham số, ko có tham số
  //   hàm có tham số => dùng arrow function để bọc lại
  render() {
    return (
      <div className="text-center">
        <button onClick={this.handleDecrease} className="btn btn-danger">
          -
        </button>
        <span className="display-4 mx-5">{this.state.number}</span>
        <button onClick={this.handleIncrease} className="btn btn-warning">
          +
        </button>
        <h2 className={`display-1   ${this.state.user == "Alice" ? "text-secondary " : "text-primary"}`}>{this.state.user}</h2>
        <button
          onClick={() => {
            this.handleChangeName("Bob");
          }}
          className="btn btn-primary"
        >
          <h2 className="titile">Change to Bob</h2>
        </button>
        <button
          onClick={() => {
            this.handleChangeName("Alice");
          }}
          className="btn btn-secondary"
        >
          Change to Alice
        </button>
      </div>
    );
  }
}
